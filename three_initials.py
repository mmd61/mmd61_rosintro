#! /usr/bin/env python
scale = 1.0
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()    
move_group = moveit_commander.MoveGroupCommander("manipulator")

print("Reference frame: %s" % move_group.get_planning_frame())
print("End effector: %s" % move_group.get_end_effector_link())
print("Robot Groups:")
print(robot.get_group_names())
print("Current Joint Values: ")
print(move_group.get_current_joint_values())
print("Current Pose: ")
print(move_group.get_current_pose())
print("Robot State:")
print(robot.get_current_state())

# This block sets a new pose goal for the robot to make sure that the joints of the 
# robot are in thre correct congiration to draw the letters. Without setting this pose
# first, the robot's joints are not able to follow the Cartesian Path given later 
# in the scipt.
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.1
pose_goal.position.z = 0.4

move_group.set_pose_target(pose_goal)

plan = move_group.go(wait=True)
# Calling `stop()` ensures that there is no residual movement
move_group.stop()
# It is always good to clear your targets after planning with poses.
# Note: there is no equivalent function for clear_joint_value_targets()
move_group.clear_pose_targets()
# ------- END OF BLOCK --------

# This block sets a new pose goal for the robot to move the end effector to the bottom left
# hand corner of the rectangle the letters will be drawn in. 
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.0
pose_goal.position.z = 0.1

move_group.set_pose_target(pose_goal)

plan = move_group.go(wait=True)
# Calling `stop()` ensures that there is no residual movement
move_group.stop()
# It is always good to clear your targets after planning with poses.
# Note: there is no equivalent function for clear_joint_value_targets()
move_group.clear_pose_targets()
# ------- END OF BLOCK --------

# The next block of code utilizes the Cartesian Paths part of the tutorial to draw 
# four seperate lines that form each part of the letter "M", then the letter "M" 
# again, and finally the letter "D". I used Cartesian Paths instead of pose goals 
# so that the end effector would follow a specific path and would not twist and 
# take an undesirable path from one point to another.
waypoints = []

# First letter "M"
wpose = move_group.get_current_pose().pose
wpose.position.z += scale * 0.4  # Move up 0.4 in z
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.2 # Move down 0.2 in z ....
wpose.position.y += scale * 0.13  # and right 0.13 in y to form a diagonal line
waypoints.append(copy.deepcopy(wpose))

wpose.position.z += scale * 0.2 # Move up 0.2 in z ....
wpose.position.y += scale * 0.13  # and right 0.13 in y to form a second diagonal line
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.4  # Move down 0.4 in z to finish letter
waypoints.append(copy.deepcopy(wpose))

# Reset to starting position
wpose.position.y -= scale * 0.26  # move left 0.26 to reset to starting position
waypoints.append(copy.deepcopy(wpose))

# Second letter "M"
wpose = move_group.get_current_pose().pose
wpose.position.z += scale * 0.4  # Move up 0.4 in z
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.2 # Move down 0.2 in z ....
wpose.position.y += scale * 0.13  # and right 0.13 in y to form a diagonal line
waypoints.append(copy.deepcopy(wpose))

wpose.position.z += scale * 0.2 # Move up 0.2 in z ....
wpose.position.y += scale * 0.13  # and right 0.13 in y to form a second diagonal line
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.4  # Move down 0.4 in z to finish letter
waypoints.append(copy.deepcopy(wpose))

# Reset to starting position
wpose.position.y -= scale * 0.26  # move left 0.26 to reset to starting position
waypoints.append(copy.deepcopy(wpose))

# Final letter "D"
wpose.position.z += scale * 0.4  # Move up 0.4 in z
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.13 # Move down 0.13 in z ....
wpose.position.y += scale * 0.26  # and right 0.26 in y to form a diagonal line
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.14  # Move down 0.14 in z
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.13 # Move down 0.13 in z ....
wpose.position.y -= scale * 0.26  # and left 0.26 in y to form a diagonal line
waypoints.append(copy.deepcopy(wpose))


# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space, which is sufficient
# for this tutorial.
(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

move_group.execute(plan, wait=True)
# ------- END OF BLOCK --------

moveit_commander.roscpp_shutdown()
