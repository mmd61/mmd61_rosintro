rosservice call /clear
rosservice call /turtle1/set_pen 255 0 0 3 1
rosservice call /turtle1/teleport_absolute 1.0 0.5 1.571
rosservice call /turtle1/set_pen 255 0 0 3 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[6.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle1/teleport_relative 0.0 3.665
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.1]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 3.66]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[6.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /spawn 5.0 0.5 1.571 'turtle2'
rosservice call /turtle2/set_pen 0 255 0 3 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[3.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle2/set_pen 0 255 0 3 1
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle2/set_pen 0 255 0 3 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
