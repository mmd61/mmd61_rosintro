import sys

import copy

import rospy

import moveit_commander

import moveit_msgs.msg

import geometry_msgs.msg



moveit_commander.roscpp_initialize(sys.argv)

rospy.init_node('move_group_python_interface_tutorial', anonymous=True)



robot = moveit_commander.RobotCommander()

scene = moveit_commander.PlanningSceneInterface()    

group = moveit_commander.MoveGroupCommander("manipulator")



Print("Reference frame: %s" % group.get_planning_frame())

Print("End effector: %s" % group.get_end_effector_link())

Print("Robot Groups:”)

Print(robot.get_group_names())

Print("Current Joint Values: »)

Print(group.get_current_joint_values())

print("Current Pose: »)

Print(group.get_current_pose())

Print("Robot State:”)

Print(robot.get_current_state())



moveit_commander.roscpp_shutdown()

/usr/bin/env: ‘python’: No such file or directory
